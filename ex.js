//Bai1: Tính tiền lương nhân viên

const btn1=document.querySelector(".btn_1")
btn1.onclick=function(){
    const luong_mot_ngay=document.querySelector(".salary_one_day").value*1
    const so_ngay_lam_viec=document.querySelector(".number_day_work").value*1
    const luong=document.querySelector(".salary")
    if(luong_mot_ngay<0||so_ngay_lam_viec<0){
    luong.innerHTML="lương 1 ngày và số ngày làm việc không được âm"
    } else {
        luong.innerHTML=luong_mot_ngay*so_ngay_lam_viec
    }
} 

//Bai2: Tính giá trị trung bình 5 số

const btn2=document.querySelector(".btn_2")
btn2.onclick=function(){
    const so_thu_nhat=document.querySelector(".number_one").value*1
    const so_thu_hai=document.querySelector(".number_two").value*1
    const so_thu_ba=document.querySelector(".number_three").value*1
    const so_thu_bon=document.querySelector(".number_four").value*1
    const so_thu_nam=document.querySelector(".number_five").value*1
    const trung_binh=document.querySelector(".average")
    trung_binh.innerHTML=(so_thu_nhat+so_thu_hai+so_thu_ba+so_thu_bon+so_thu_nam)/5
}

//Bai3: Quy đổi tiền

const btn3=document.querySelector(".btn_3")
btn3.onclick=function(){
    const tienUSD=document.querySelector(".money").value*1
    const gia_tri_quy_doi=document.querySelector(".currency")
    gia_tri_quy_doi.innerHTML=tienUSD*23500
} 

//Bai4: Quy đổi tiền

const btn4=document.querySelector(".btn_4")
btn4.onclick=function(){
    const chieu_dai=document.querySelector(".width").value*1
    const chieu_rong=document.querySelector(".height").value*1
    const chu_vi=document.querySelector(".perimeter")
    const dien_tich=document.querySelector(".area")
    chu_vi.innerHTML=(chieu_dai+chieu_rong)/2
    dien_tich.innerHTML=(chieu_dai*chieu_rong)
} 

//Bai5: Quy đổi tiền

const btn5=document.querySelector(".btn_5")
btn5.onclick=function(){
    const number=document.querySelector(".number").value*1
    const result=document.querySelector(".result")
    const hang_chuc=Math.floor(number/10)
    const don_vi=number%10
    result.innerHTML=hang_chuc+don_vi
} 